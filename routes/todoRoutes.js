var express = require('express');
var router = express.Router();

var VerifyToken = require('../services/verifyToken');
var TodoServices = require('../services/todoServices');
var User = require('../schema/user');

router.get('/list', VerifyToken.verifyToken, function(req, res) {
    User.findOne({_id:req.userId}, function(err, user){
      if(!user){
        return res.status(401).send({code: 401, message: "Unauthorized"});
      }
      if(user.is_login){
        TodoServices.findByUser(req, res);
      }else{
        return res.status(401).send({code: 401, message: "Unauthorized"});
      }
    })
});

router.get('/detail/:id', VerifyToken.verifyToken, function(req, res) {
  User.findOne({_id:req.userId}, function(err, user){
    if(!user){
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
    if(user.is_login){
      TodoServices.findById(req, res);
    }else{
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
  })
});

router.post('/add', VerifyToken.verifyToken, function(req, res) {
  User.findOne({_id:req.userId}, function(err, user){
    if(!user){
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
    if(user.is_login){
      TodoServices.add(req, res);
    }else{
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
  })
});

router.delete('/delete/:id', VerifyToken.verifyToken, function(req, res) {
  User.findOne({_id:req.userId}, function(err, user){
    if(!user){
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
    if(user.is_login){
      TodoServices.deleteById(req, res);
    }else{
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
  })
});

router.put('/update', VerifyToken.verifyToken, function(req, res) {
  User.findOne({_id:req.userId}, function(err, user){
    if(!user){
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
    if(user.is_login){
      TodoServices.update(req, res);
    }else{
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
  })
});

module.exports = router;
