var express = require('express');
var router = express.Router();

var VerifyToken = require('../services/verifyToken');
var UserServices = require('../services/userServices');
var User = require('../schema/user');

router.get('/', function(req, res, next) {
  res.send('users API');
});

router.post('/register', function(req, res){
  UserServices.register(req, res);
})

router.post('/login', function(req,res){
  UserServices.login(req,res);
})

router.post('/logout', VerifyToken.verifyToken, function(req,res){
  User.findOne({_id:req.userId}, function(err, user){
    if(!user){
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
    if(user.is_login){
      UserServices.logout(req,res);
    }else{
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
  })
})

router.get('/detail',VerifyToken.verifyToken, function(req, res){
  User.findOne({_id:req.userId}, function(err, user){
    if(!user){
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
    if(user.is_login){
      UserServices.find(req,res);
    }else{
      return res.status(401).send({code: 401, message: "Unauthorized"});
    }
  })
})

module.exports = router;
