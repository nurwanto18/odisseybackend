var mongoose = require('mongoose'); 
var autoIncrement = require('mongoose-auto-increment');

var TodoSchema = new mongoose.Schema({  
  todo_name: String,
  user_id: {type: Number, required: true}
});
autoIncrement.initialize(mongoose.connection);
TodoSchema.plugin(autoIncrement.plugin, 'user');

mongoose.model('Todo', TodoSchema);

module.exports = mongoose.model('Todo');