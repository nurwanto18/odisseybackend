var mongoose = require('mongoose'); 
var autoIncrement = require('mongoose-auto-increment');

var UserSchema = new mongoose.Schema({  
  username: String,
  password: String,
  is_login: Boolean
});
autoIncrement.initialize(mongoose.connection);
UserSchema.plugin(autoIncrement.plugin, 'user');

mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');