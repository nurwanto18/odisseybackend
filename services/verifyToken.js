var jwt = require('jsonwebtoken');
var config = require('../config/config');
var User = require('../schema/user');

function verifyToken(req, res, next) {

  var token = req.headers['x-access-token'];
  if (!token) 
    return res.status(403).send({ code: 401, message: 'No token provided.' });

  jwt.verify(token, config.secret, function(err, decoded) {      
    if (err) 
      return res.status(500).send({ code: 401, message: 'Failed to authenticate token.' });    

    req.userId = decoded.id;
    next();
  });

}

function verifyIsLogin(userId){
  User.find({_id:userId}, function(err, user){
    if(!user){
      return false
    }

    if(user.is_login){
      return true
    }
  })
  
  return false
}

module.exports = {verifyToken, verifyIsLogin};