var Todo = require('../schema/todo');
var VerifyToken = require('../services/verifyToken');

function findByUser (req, res){
    Todo.find({ 
        user_id : req.userId
      }, 
      function (err, todo) {
        if (err) {
          return res.status(500).send({code: 500, message: "Internal Server Error"});
        }
    
        if(!todo && todo.length < 1){
            return res.status(404).send({ code: 404, message: "DATA NOT FOUND"});
        }
        
        return res.status(200).send({ code: 200, message: "TODO IS FOUND", data: todo });
      });
}

function findById (req, res){
    if(req.params.id == undefined || req.params.id == null){
      return res.status(403).send({code: 403, message: "Bad request"});
    }
    Todo.findOne({ 
      _id : req.params.id,
      user_id : req.userId
    }, 
    function (err, todo) {
      if (err) {
        return res.status(500).send({code: 500, message: "Internal Server Error"});
      }
  
      if(todo == null){
        return res.status(404).send({ code: 404, message: "DATA NOT FOUND"});
    }
      return res.status(200).send({ code: 200, message: "TODO IS FOUND", data: todo });
    });
}

function add(req, res){
    if(req.body.todo == undefined || req.body.todo == null){
      return res.status(403).send({code: 403, message: "Bad request"});
    }
    Todo.create({ 
      todo_name : req.body.todo,
      user_id : req.userId
    }, 
    function (err, todo) {
      if (err) {
        return res.status(500).send({code: 500, message: "Internal Server Error"});
      }
      return res.status(200).send({ code: 200, message: "CREATE SUCCESS" });
    });
}

function deleteById (req,res){
    if(req.params.id == undefined || req.params.id == null){
      return res.status(403).send({code: 403, message: "Bad request"});
    }
    Todo.findOneAndRemove({
        _id : req.params.id,
        user_id : req.userId
    }, 
    function (err, todo) {
        if (err) {
          return res.status(500).send({code: 500, message: "Internal Server Error"});
        }

        if(todo == null){
            return res.status(404).send({ code: 404, message: "DATA NOT FOUND"});
        }
        return res.status(200).send({ code: 200, message: "DELETE SUCCESS" });
    })
}

function update(req,res){
    if(req.body.id== undefined || req.body.id == null){
      return res.status(403).send({code: 403, message: "Bad request"});
    }
    if(req.body.todo== undefined || req.body.todo == null){
      return res.status(403).send({code: 403, message: "Bad request"});
    }

    Todo.findOneAndUpdate({
        _id : req.body.id,
        user_id : req.userId
    }, {
        todo_name : req.body.todo
    }, 
    function (err, todo) {
        if (err) {
          return res.status(500).send({code: 500, message: "Internal Server Error"});
        }

        if(todo == null){
            return res.status(404).send({ code: 404, message: "DATA NOT FOUND"});
        }
        return res.status(200).send({ code: 200, message: "UPDATE SUCCESS"});
    })
}

module.exports = { findByUser, findById, add, deleteById, update };