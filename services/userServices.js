var User = require('../schema/user');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config/config');

function find (req, res){
    User.find({ 
      _id : req.userId
    }, 
    function (err, user) {
      if (err) {
        return res.status(500).send({code: 500, message: "Internal Server Error"});
      }
  
      if(user.length < 1){
        return res.status(200).send({ code: 200, message: "USER IS NOT FOUND" });
      }
      return res.status(200).send({ code: 200, message: "USER IS FOUND", data: user });
    });
}

function register(req, res){
    if(req.body.username == undefined || req.body.username == null){
      return res.status(403).send({code: 403, message: "Bad request"});
    }
    if(req.body.password == undefined || req.body.password == null){
      return res.status(403).send({code: 403, message: "Bad request"});
    }

    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
  
    User.create({ 
      username : req.body.username,
      password : hashedPassword,
      is_login:false
    }, 
    function (err, user) {
      if (err) {
        return res.status(500).send({code: 500, message: "Internal Server Error"});
      }
      return res.status(200).send({ code: 200, message: "SUCCESS" });
    });
}

function login (req, res){
    User.findOne({ username: req.body.username }, function (err, user) {
        if (err){
          return res.status(500).send({code: 500, message: "Internal Server Error"});
        } 
        if (!user){
          return res.status(401).send({ code: 401, message: "AUTHENTICATION FAILED" });
        } 
        
        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid){
          return res.status(401).send({ code: 401, message: "AUTHENTICATION FAILED" });
        } 
    
        var token = jwt.sign({ id: user._id }, config.secret, {
          expiresIn: 300
        });

        console.log(user._id)
        User.findByIdAndUpdate({_id: user._id }, {is_login:true}, function (err, user) {
          if (err) {
            return res.status(500).send({code: 500, message: "Internal Server Error"});
          }
  
          if(user.length < 1){
              return res.status(404).send({ code: 404, message: "DATA NOT FOUND"});
          }
          res.status(200).send({ code: 200, message: "LOGIN SUCCESS", token: token });
        })
    });
}

function logout (req, res){
    User.findByIdAndUpdate({_id: req.userId }, {is_login:false}, function(err, user){
        if (err){
            return res.status(500).send({code: 500, message: "Internal Server Error"});
        } 
        if (!user){
            return res.status(401).send({ code: 401, message: "AUTHENTICATION FAILED" });
        } 

        res.status(200).send({ code: 200, message: "LOGOUT SUCCESS"});
    })
    
}

module.exports = { find, register, login, logout };